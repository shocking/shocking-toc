from collections import namedtuple
from re import compile

from bs4 import BeautifulSoup

from shocking.plugin import BasePlugin

# "toc" = "shocking_toc:TocPlugin"
# "group-toc" = "shocking_toc:GroupTocPlugin"

Header = namedtuple("Header", ["name", "id", "level", "parent", "children"])


def normalize_name(string):
    """ Normalize name for URL usage """
    return string.lower().replace(' ', '-')


class TocPlugin(BasePlugin):
    """ Generate individual ToC for each page in a group """
    _defaults = {
        "ordered": True,
        "links": True,
        "min_depth": 2,
        "max_depth": 6,
    }

    def prepare(self):
        # Prepare the regex for which header levels we would like to list
        self.regex = compile("^h[{min_depth}-{max_depth}]$".format(**self.config))

        print(self.config)

    def process(self, file_object, **metadata):
        document = BeautifulSoup(file_object.content, features="html.parser")

        header_elements = document.find_all(self.regex)

        root = self._get_header_tree(header_elements)

        html_list = self._get_header_list(document, root)

        # Save the HTML list in the page's metadata
        file_object.partials["toc"] = html_list.prettify()

        return file_object

    def _get_header_tree(self, header_elements):
        # Create a root Header node for our tree
        root = last_node = Header(None, '', 0, None, [])

        def get_parent(node_level):
            previous_node = last_node

            # Work up the tree until root node or parent of lower level
            while previous_node and not previous_node.level < node_level:
                previous_node = previous_node.parent

            return previous_node

        for element in header_elements:
            level, content = int(element.name[-1]), str(element.string)

            parent = get_parent(level)

            new_node = Header(content, normalize_name(content), level, parent, [])

            parent.children.append(new_node)

            # Set id so anchors can link to headers
            try:
                element['id'] = new_node.id
            except KeyError:
                new_node._replace("id", element["id"])

            last_node = new_node

        return root

    def _get_header_list(self, document, root_node):
        list_type = "ol" if self.config["ordered"] else "ul"

        # Create root list element for toc fragment
        toc_list = document.new_tag(list_type, id="toc")

        def get_list_item(node):
            list_item = document.new_tag("li")

            if self.config["links"]:
                anchor = document.new_tag("a", href=f"#{node.id}")

                anchor.string = node.name

                list_item.append(anchor)
            else:
                list_item.string = node.name

            if node.children:
                child_list = document.new_tag(list_type)

                for child_node in node.children:
                    child_list.append(get_list_item(child_node))

                list_item.append(child_list)

            return list_item

        for child_node in root_node.children:
            toc_list.append(get_list_item(child_node))

        return toc_list


class GroupTocPlugin(BasePlugin):
    pass
